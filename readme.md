# k4te a kubernetes cluster on rpi 3b+

```text
 __      _____   __
|  | __ /  |  |_/  |_  ____  
|  |/ //   |  |\   __\/ __ \
|    </    ^   /|  | \  ___/
|__|_ \____   | |__|  \___  >
     \/    |__|           \/


Steps to setup a kubernetes Cluster on RPI

Network: 10.1.1.0/24

Gateway: 10.1.1.1
DNS: 10.1.1.1.1 (running dnsmasq on AdvancedTomato Router)
Router DHCP range: 10.1.1.101 - 10.1.1.200


* 10.1.1.1 - Router
* 10.1.1.2 - Managed Switch

Kubernetes Nodes:
    - k4te (master): 10.1.1.14
    - k3te (node3): 10.1.1.13
    - k2te (node2): 10.1.1.12
    - k1te (node1): 10.1.1.11

MetalLB CIDR: 10.1.1.16/28
    - 10.1.1.17 - 10.1.1.30

Traefik Internal Ingress IP: 10.1.1.20
Traefik External Ingress IP: 10.1.1.21
```

## Install Rasbian lite

Install https://etcher.io. afterwards Download and flash the latest Rasbian lite:

```sh
curl -fsSL https://downloads.raspberrypi.org/raspbian_lite_latest -o raspbian_lite.zip
```

After you flashed the sdcards put a `ssh`file to the `/boot` partition, to allow headless installation.

## init.sh

Init raspbian-stretch-lite:

- Set hostname
- Set locale to en_US
- Set timezone to Europe/Zurich
- Set static IP address
- Create user `hey`and write ssh key to `~/.shh/authorized_keys`
- Adds user to sudoers file and sudo group

Download the script to the pi:
`curl -sSLO https://gitlab.com/andybroger/k4te/raw/master/scripts/init.sh && chmod +x init.sh`

run script:

```sh
init.sh <name> <ip/mask>
exp. init.sh 10.1.1.11/24
```

# install_k8s.sh

Installs docker and kubernetes tools.
Adds " cgroup_enable=cpuset cgroup_enable=memory" to /boot/cmdline.txt
`curl -sSL https://gitlab.com/andybroger/k4te/raw/master/scripts/install_k8s.sh | sh`

## init kubernetes cluster

Normally Kubernetes waits 5 minutes to reschedule a pod, when its crashed.
Since this rack is also for demo, set it to 10s.

`vi kubeadm_conf.yml`

```yaml
apiVersion: kubeadm.k8s.io/v1alpha1
kind: MasterConfiguration
networking:
 podSubnet: 10.244.0.0/16
controllerManagerExtraArgs:
  pod-eviction-timeout: 10s
  node-monitor-grace-period: 10s
```

save and run it:

`sudo kubeadm init --config k8s/kubeadm_conf.yaml`

wait till the master is up and running and follow the instructions:

```shell
  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

## Setup container network

Install Fannel to setup the Mesh Network between containers, run this on the master node

```shell
kubectl apply -f https://gitlab.com/andybroger/k4te/raw/master/k8s/kube-fannel.yaml
```

## Untaining Master

If you want to let the Scheduler also use the Master node to run pods run `kubectl taint nodes --all node-role.kubernetes.io/master-`this will remove the master role on all masters of the cluster.

## join workers

let the workers join
`sudo kubeadm join 10.1.1.14:6443 --token TOKEN --discovery-token-ca-cert-hash HASH`

## Install kubectl on remote machine and admin the cluster

`scp hey@k4.local:/home/hey/.kube/config ~/.kube/`

## add persistent storage

```shell
sudo apt-get install nfs-kernel-server nfs-common
sudo systemctl enable nfs-kernel-server
sudo mkdir -p /var/data/kube

echo "/var/data/kube/ 10.1.1.*(rw,sync,no_subtree_check,no_root_squash)" | sudo tee -a /etc/exports > /dev/null
sudo exportfs -a

#change settings in yaml files
kubectl apply -f nfs/rbac.yaml
kubectl apply -f nfs/class.yaml
kubectl apply -f nfs/deployment-arm.yaml
kubectl patch storageclass nfs-disk-node1 -p '{"metadata":{"annotations": {"storageclass.kubernetes.io/is-default-class": "true"}}}'
```

## install metal LB

https://metallb.universe.tf/installation/

Change IP Subnet in metallb-conf.yaml!

```shell
kubectl apply -f https://raw.githubusercontent.com/google/metallb/v0.6.2/manifests/metallb.yaml
kubectl apply -f metallb-conf.yaml
```

## install traefik

Change IP's and domains in the yaml files!

```shell
kubectl apply -f traefik/traefik-rbac.yml
kubectl apply -f traefik/traefik-internal-configmap.yaml
kubectl apply -f traefik/traefik-internal-deployment.yaml
kubectl apply -f traefik/traefik-internal-service.yaml
```

## setup dashboard

```shell
kubectl apply -f dashboard/dashboard-ingress.yaml
kubectl apply -f dashboard/dashboard.yaml
```

## setup heapster

```shell
kubectl apply -f heapster-influx/heapster-rbac.yaml
kubectl apply -f heapster-influx/heapster.yaml
```

## install helm

```shell
brew install kubernetes-helm
kubectl create -f helm/rbac-config.yaml
helm init --service-account tiller --tiller-image timotto/rpi-tiller
```

## Add Loadbalancer/Proxy

Traefik?

## Add CI/CD to K4te

Jenkins on arm?
