#!/bin/sh

#vars
hostname=$1
ip=$2 # exp. 192.168.1.100/24
ssh_key="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDzyrpmRWJvbHs9Q6DVE3H501jdZ/SNHTd4K2W95urDzNnOem5p19qZeEHdK3toCgH6gKQmtZFq9mV+LVcm2vn/7kAgCXq2Dh42LNXyAAMYznEs2lbRCb30F3ILqTpod9nFnkbvRfFzwJ45wplSaCKu8zXxYUFBae9/Uza7Kl+2Fsh1s9ixb89QgoflEZydDVNVOw6vcQPfw0MyX7b80WXXSIRrjrvr1VrwRTRswzkyB47+J0mTXc7stfADIQZ19nrnfOlFbpt1iRdLlATL1QQZVzl2LBpqyVUHJTfOTrM1GZomMJeUqHeivPWVQP/6ug+EO4fMOEQeD7BinVYjpqJf andybroger@Andys-MacBook-Pro.local"
int=eth0
router=10.1.1.1
dns=10.1.1.1
timezone=Europe/Zurich

# check hostname and ip/mask
if [ -z "$hostname" ] || [ -z "$ip" ]; then
    echo "./init.sh <hostname> <ip/mask>" 1>&2
    echo "./init.sh master1 10.1.1.10/24" 1>&2
    exit 1
fi

#Set Locale
sudo sed -i -e 's/# en_US.UTF-8/en_US.UTF-8/g' /etc/locale.gen
sudo locale-gen

sudo tee /etc/default/locale <<EOT > /dev/null
LANG=en_US.UTF-8
LC_ALL=en_US.UTF-8
LANGUAGE=en_US.UTF-8
EOT

#Set timezone
sudo timedatectl set-timezone $timezone

# Change the hostname
sudo hostnamectl --transient set-hostname $hostname
sudo hostnamectl --static set-hostname $hostname
sudo hostnamectl --pretty set-hostname $hostname
sudo sed -i s/raspberrypi/$hostname/g /etc/hosts

# Set the static ip

sudo tee -a /etc/dhcpcd.conf <<EOT > /dev/null
interface $int
static ip_address=$ip
static routers=$dns
static domain_name_servers=$dns
EOT

#set motd
sudo tee /etc/motd <<EOT > /dev/null
 __      _____   __
|  | __ /  |  |_/  |_  ____  
|  |/ //   |  |\   __\/ __ \
|    </    ^   /|  | \  ___/
|__|_ \____   | |__|  \___  >
     \/    |__|           \/
            $hostname.h.brgr.xyz
EOT

#add user hey
sudo useradd -m -s /bin/bash hey
sudo addgroup hey sudo
sudo mkdir -p /home/hey/.ssh
sudo touch /home/hey/.ssh/authorized_keys
sudo chown -R hey:hey /home/hey/.ssh
echo "hey ALL=NOPASSWD: ALL" | sudo tee -a /etc/sudoers > /dev/null
echo $ssh_key | sudo tee -a /home/hey/.ssh/authorized_keys > /dev/null

tee /dev/null <<EOT
init.sh: locale, timezone, hostname, static ip is set.
init.sh: now 'sudo reboot and login with the new user 'hey'
init.sh after that 'sudo userdel -r -f pi'

init.sh: Warning!!
init.sh: if you didn't set the ssh_key var in
init.sh: this script, don't forget to write your ssh
init.sh: keys into /home/hey/.ssh/authorized_keys.
EOT